#!/usr/bin/ruby
require 'cinch'
require 'trello'
require 'json'
require 'yaml'
require 'resolv'
require_relative './mailer.rb'

$board = nil
$add_cards_list = nil

config_file = ARGV.shift || "config.yml"
if not File.exists? config_file
  puts "Can't find config file #{config_file}"
  puts "Either create it or specify another config file with: #{File.basename $0} [filename]"
  exit
end

$config = YAML.load_file config_file
$botnick = $config["irc"]["nick"] ##FIXME: Use the actual bots current nick, not the configured nick.
TRELLO_ADD_CARDS_LIST = $config["trello"]["add_cards_list"] ||= "To Do"
TRELLO_BOARD_ID = $config["trello"]["board_id"]

TRELLO_API_KEY = $config["trello"]["api"]["key"]
TRELLO_API_SECRET = $config["trello"]["api"]["secret"]
TRELLO_API_ACCESS_TOKEN_KEY = $config["trello"]["api"]["access_token_key"]
TRELLO_API_KEY_OWNER_HOST = $config["trello"]["api"]["key_owner_host"]

include Trello
include Trello::Authorization

Trello::Authorization.const_set :AuthPolicy, OAuthPolicy
OAuthPolicy.consumer_credential = OAuthCredential.new TRELLO_API_KEY, TRELLO_API_SECRET
OAuthPolicy.token = OAuthCredential.new TRELLO_API_ACCESS_TOKEN_KEY, nil

def nick_hacks(username)
  if $username == "elfy"
    $username = "elfy1"
  elsif $username == "ochosi"
    $username = "simonsteinbeiss"
  elsif $username == "knome"
    $username = "knomepasi"
  elsif $username == "pleia2"
    $username = "elizabethkrumbach"
  end
end

def given_short_id_return_long_id(board, short_id)
  long_ids = board.cards.collect { |c| c.id if c.url.match(/\/(\d+).*$/)[1] == short_id.to_s}
  long_ids.delete_if {|e| e.nil?}
end

def get_list_by_name(name)
  $board.lists.find_all {|l| l.name.casecmp(name.to_s) == 0}
end

def validate_mail(email)
  unless email.blank?
    unless email =~ /^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$/
      raise "Your email address does not appear to be valid."
    else
      raise "Your email domain name appears to be incorrect." unless validate_email_domain(email)
    end
  end
end

def validate_email_domain(email)
  domain = email.match(/\@(.+)/)[1]
  Resolv::DNS.open do |dns|
    @mx = dns.getresources(domain, Resolv::DNS::Resource::IN::MX)
  end
  @mx.size > 0 ? true : false
end

def sync_board
  return $board.refresh! if $board
  $board = Trello::Board.find(TRELLO_BOARD_ID)
  $add_cards_list = $board.lists.detect { |l| l.name.casecmp(TRELLO_ADD_CARDS_LIST) == 0 }
end

def say_help(m)
  m.user.send "I can tell you the open cards on the lists on your Trello board. Just address me with the name of the list (it's not case sensitive)."
  m.user.send "For example - #{$botnick}: ideas"
  m.user.send "I also understand the following commands:"
  m.user.send "  ->  help - display this help"
  m.user.send "  ->  source - link to #{$botnick}'s source"
  m.user.send "  ->  sync - re-syncs the local cache with the Trello board"
  m.user.send "  ->  lists - show all the board list names"
  m.user.send "  ->  due soon - return all cards due within the next two weeks"
  m.user.send "  ->  card add this is a card - creates a new card named: \'this is a card\' in a list defined in the add_cards_list config variable or if it\'s not present in a list named To Do"
  m.user.send "  ->  card <id> comment this is a comment on card <id> - creates a comment on the card with short id equal to <id>"
  m.user.send "  ->  card <id> move to Doing - moves the card with short id equal to <id> to the list Doing"
  m.user.send "  ->  card <id> add member joe - assign joe to the card with short id equal to <id>"
  m.user.send "  ->  card <id> view joe@email.com - sends an email to joe@email.com with the content of the card with short id equal to <id>"
  m.user.send "  ->  card <id> link - return a link to the card with short id equal to <id>"
  m.user.send "  ->  cards joe - return all cards assigned to joe"
end

bot = Cinch::Bot.new do
  configure do |c|
    c.nick = $config["irc"]["nick"]
    c.user = "trello"
    c.realname = "Trello Bot"
    c.server = $config["irc"]["server"]
    c.port = $config["irc"]["port"]
    c.ssl.use = $config["irc"]["ssl"]
    c.ssl.client_cert = $config["irc"]["auth"]["certfp"]
    c.sasl.username = $config["irc"]["auth"]["user"]
    c.sasl.password = $config["irc"]["auth"]["pass"]
    c.channels = $config["irc"]["channels"]
    quit_code = $config["irc"]["quit_code"]
  end
  sync_board

  # trellobot is polite, and will only reply when addressed
  addressed = /^#{$botnick}_*[:,] (.*)/
  on :message, addressed do |m|  ##TODO: Register on 'botnick[:,]' or in PM.
    # if trellobot can't get thru to the board, then send the human to the human url
    sync_board unless $board
    unless $board
      m.reply "I can't seem to get the list of ideas from Trello, sorry. Try here: https://trello.com/board/#{TRELLO_BOARD_ID}"
      bot.halt
    end

    # trellobot: what up?  <- The bit we are interested in is past the ':'
    searchfor = addressed.match(m.message)[1]
    searchfor = searchfor.strip.downcase

    case searchfor
    when /(hi|hello|howdy)/
      greeting = searchfor.match(/(hi|hello|howdy|hey)/)
      m.reply "#{greeting[1].capitalize}, #{m.user.nick}."
    when /^card add/
     unless m.user.host == TRELLO_API_KEY_OWNER_HOST
        m.reply "Write mode disabled."
     else
      if $add_cards_list.nil?
        m.reply "Can't add card. It wasn't found any list named: #{TRELLO_ADD_CARDS_LIST}."
      else
        name = searchfor.strip.match(/^card add (.+)$/)[1]
        card = Trello::Card.create(:name => name, :list_id => $add_cards_list.id)
        m.reply "Created card #{card.name} with id: #{card.short_id}."
      end
     end
    when /^card \d+ comment/
     unless m.user.host == TRELLO_API_KEY_OWNER_HOST
        m.reply "Write mode disabled."
     else
      card_regex = searchfor.match(/^card (\d+) comment (.+)/)
      card_id = given_short_id_return_long_id($board, card_regex[1])
      if card_id.count == 0
        m.reply "Couldn't find any card with id: #{card_regex[1]}. Aborting."
      elsif card_id.count > 1
        m.reply "There are #{list.count} cards with id: #{regex[1]}. Don't know what to do. Aborting"
      else
        comment = card_regex[2]
        card = Trello::Card.find(card_id[0].to_s)
        card.add_comment comment
        m.reply "Added \"#{comment}\" comment to \"#{card.name}\" card."
      end
     end
    when /^card \d+ move to \w+/
     unless m.user.host == TRELLO_API_KEY_OWNER_HOST
        m.reply "Write mode disabled."
     else
      regex = searchfor.match(/^card (\d+) move to (\w+)/)
      list = get_list_by_name(regex[2].to_s)
      card_id = given_short_id_return_long_id($board, regex[1].to_s)
      if card_id.count == 0
        m.reply "Couldn't find any card with id: #{regex[1]}. Aborting."
      elsif card_id.count > 1
        m.reply "There are #{list.count} cards with id: #{regex[1]}. Don't know what to do. Aborting."
      else
        if list.count == 0
          m.reply "Couldn't find any list named: \"#{regex[2].to_s}\". Aborting."
        elsif list.count > 1
          m.reply "There are #{list.count} lists named: #{regex[2].to_s}. Don't know what to do. Aborting."
        else
          card = Trello::Card.find(card_id[0])
          list = list[0]
          if card.list.name.casecmp(list.name) == 0
            m.reply "Card \"#{card.name}\" is already on list \"#{list.name}\"."
          else
            card.move_to_list list
            m.reply "Moved card \"#{card.name}\" to list \"#{list.name}\"."
          end
        end
      end
     end
    when /^card \d+ add member \w+/
     unless m.user.host == TRELLO_API_KEY_OWNER_HOST
        m.reply "Write mode disabled."
     else
      regex = searchfor.match(/^card (\d+) add member (\w+)/)
      $username = regex[2]
      nick_hacks($username)
      card_id = given_short_id_return_long_id($board, regex[1].to_s)
      if card_id.count == 0
        m.reply "Couldn't find any card with id: #{regex[1]}. Aborting."
      elsif card_id.count > 1
        m.reply "There are #{list.count} cards with id: #{regex[1]}. Don't know what to do. Aborting."
      else
        card = Trello::Card.find(card_id[0])
        membs = card.members.collect {|m| m.username}
        begin
          member = Trello::Member.find($username)
        rescue
          member = nil
        end
        if member.nil?
          m.reply "User \"#{$username}\" doesn't exist in Trello."
        elsif membs.include? $username
          m.reply "#{member.full_name} is already assigned to card \"#{card.name}\"."
        else
          card.add_member(member)
          m.reply "Added \"#{member.full_name}\" to card \"#{card.name}\"."
        end
      end
     end
    when /^cards \w+/
      $username = searchfor.match(/^cards (\w+)/)[1]
      nick_hacks($username)
      cards = []
      $board.cards.each do |card|
        members = card.members.collect { |mem| mem.username }
        if members.include? $username
          cards << card
        end
      end
      inx = 1
      if cards.count == 0
        m.reply "User \"#{$username}\" has no cards assigned."
      end
      cards.each do |c|
        if c.due.nil?
          m.reply "  ->  #{inx.to_s}. #{c.name} (id: #{c.short_id}) from list: #{c.list.name}"
        else
          m.reply "  ->  #{inx.to_s}. #{c.name} (id: #{c.short_id}) due: #{c.due} from list: #{c.list.name}"
        end
        inx += 1
      end
    when /^card \d+ view (.+)/
      regex = searchfor.match(/^card (\d+) view (.+)/)
      card_id = given_short_id_return_long_id($board, regex[1].to_s)
      if card_id.count == 0
        m.reply "Couldn't find any card with id: #{regex[1]}. Aborting."
      elsif card_id.count > 1
        m.reply "There are #{list.count} cards with id: #{regex[1]}. Don't know what to do. Aborting."
      else
        card = Trello::Card.find(card_id[0])
        msg_err = nil
        begin
          validate_mail(regex[2])
        rescue => e
          msg_err = e.message
        end
        if msg_err.nil?
          begin
            email = CardMailer.send_card(regex[2], card)
            email.deliver
          rescue => e
            m.reply e.message
            m.reply "An error ocurred sending the mail. Sorry for the inconvenience."
            break
          end
          m.reply "Mailed the card \"#{card.name}\" to #{regex[2]}"
        else
          m.reply msg_err
        end
      end
    when /^card \d+ link/
      regex = searchfor.match(/^card (\d+) link/)
      card_id = given_short_id_return_long_id($board, regex[1].to_s)
      if card_id.count == 0
        m.reply "Couldn't find any card with id: #{regex[1]}. Aborting."
      elsif card_id.count > 1
        m.reply "There are #{list.count} cards with id: #{regex[1]}. Don't know what to do. Aborting."
      else
        card = Trello::Card.find(card_id[0])
        m.reply card.url
      end
    when /^due soon/
      cards = []
      $board.cards.each do |card|
        unless card.due.nil?
          due = card.due.to_s
          duedate = Time.parse(due)
          start = Time.now.utc
          enddate = Time.now.utc + (2*7*24*60*60)
          if (start..enddate).cover?(duedate)
            puts "#{card.name} (id: #{card.short_id}) due: #{card.due} from list: #{card.list.name}"
            cards << card
          end
        end
      end
      inx = 1
      if cards.count == 0
        m.reply "No cards due within the next two weeks."
      end
      cards.each do |c|
        m.reply "  ->  #{inx.to_s}. #{c.name} (id: #{c.short_id}) due: #{c.due} from list: #{c.list.name}"
        inx += 1
      end
    when /lists/
      $board.lists.each { |l|
        m.reply "  ->  #{l.name}"
      }
    when /help/
      say_help(m)
    when /\?/
      say_help(m)
    when /sync/
      sync_board
      m.reply "Ok, synced the board, #{m.user.nick}."
    when /source/
      m.reply "My source is at https://bitbucket.org/unit193/trellobot/, forked from https://github.com/oisin/trellobo"
    else
      if searchfor.length > 0
        # trellobot presumes you know what you are doing and will attempt
        # to retrieve cards using the text you put in the message to him
        # at least the comparison is not case sensitive
        list = $board.lists.detect { |l| l.name.casecmp(searchfor) == 0 }
        if list.nil?
          m.reply "There's no list called <#{searchfor}> on the board, #{m.user.nick}. Sorry."
        else
          cards = list.cards
          if cards.count == 0
            m.reply "Nothing doing on that list today, #{m.user.nick}."
          else
            ess = (cards.count == 1) ? "" : "s"
            m.reply "I have #{cards.count} card#{ess} today in list #{list.name}"
            inx = 1
            cards.each do |c|
              membs = c.members.collect {|m| m.full_name }
              if membs.count == 0
                if c.due.nil?
                  m.reply "  ->  #{inx.to_s}. #{c.name} (id: #{c.short_id})"
                else
                  m.reply "  ->  #{inx.to_s}. #{c.name} (id: #{c.short_id}) due: #{c.due}"
                end
              else
                if c.due.nil?
                  m.reply "  ->  #{inx.to_s}. #{c.name} (id: #{c.short_id}) (members: #{membs.to_s.gsub!("[","").gsub!("]","").gsub!("\"","")})"
                else
                  m.reply "  ->  #{inx.to_s}. #{c.name} (id: #{c.short_id}) due: #{c.due} (members: #{membs.to_s.gsub!("[","").gsub!("]","").gsub!("\"","")})"
                end
              end
              inx += 1
            end
          end
        end
      else
        say_help(m)
      end
    end
  end

  # if trellobot loses his marbles, it's easy to disconnect him from the server
  # note that if you are doing a PaaS deploy, he may respawn depending on what
  # the particular hosting env is (e.g. Heroku will start him up again)
  on :private, /^quit(\s*)(\w*)/ do |m, blank, code|
    bot.quit if quit_code.eql?(code)

    if code.empty?
      m.reply "There is a quit code required for this bot, sorry."
    else
      m.reply "That is not the correct quit code required for this bot, sorry."
    end
  end
end

bot.start
