require 'action_mailer'

def to_boolean(str)
  str == "true"
end

config_file = ARGV.shift || "config.yml"
if not File.exists? config_file
  puts "Can't find config file #{config_file}"
  puts "Either create it or specify another config file with: #{File.basename $0} [filename]"
  exit
end

$config = YAML.load_file config_file

MAIL_FROM = $config["mail"]["from"]
MAIL_ADDRESS = $config["mail"]["address"]
MAIL_PORT = $config["mail"]["port"]
MAIL_AUTHENTICATION = $config["mail"]["auth"]
MAIL_USERNAME = $config["mail"]["user"]
MAIL_PASSWORD = $config["mail"]["pass"]
MAIL_ENABLE_STARTTLS_AUTO = $config["mail"]["starttls_auto"]

ActionMailer::Base.raise_delivery_errors = true
ActionMailer::Base.delivery_method = :smtp
ActionMailer::Base.smtp_settings = {
  :address   => MAIL_ADDRESS,
  :port      => MAIL_PORT.to_i,
  :authentication => MAIL_AUTHENTICATION.to_sym,
  :user_name      => MAIL_USERNAME,
  :password       => MAIL_PASSWORD,
  :enable_starttls_auto => to_boolean(MAIL_ENABLE_STARTTLS_AUTO)
}
ActionMailer::Base.view_paths= File.dirname(__FILE__)

class CardMailer < ActionMailer::Base
  def send_card(to, card)
    @card = card
    @checkedlist = card.actions.collect {|a| {:id => a.data["checkItem"]["id"], :status => a.data["checkItem"]["state"]} if a.type == "updateCheckItemStateOnCard"}.delete_if {|t| t.nil?}
    @comments = card.actions.collect {|a| a if a.type == "commentCard"}.delete_if {|b| b.nil?}
    mail(:to => to.to_s, :from => MAIL_FROM, :subject => "Trellobot Card: \"#{@card.name}\"") do |format|
      format.html
    end
  end
end
